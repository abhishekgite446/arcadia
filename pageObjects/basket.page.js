const assert = require('assert');
class basket  {

    /* page elements */

    get quantitySizeLabel()   { return browser.elements('.OrderProducts-label'); }
    get basketProductTitle() {return browser.element('.OrderProducts-productName'); }
    get deleteIcon()   {return browser.element('.OrderProducts-deleteIcon');  }
    get deleteButtonPopUp() {return browser.element('.Button.OrderProducts-deleteButton') }
    get closeIconPopUp() {return browser.element('.Button');}
    get basketEmptyMsg() {return browser.element('.MiniBag-emptyLabel')}


    /* page methods */

    // This method stored the title of the product in a global variable and returns the title from basket page
    getBasketProductTitle() {
        this.basketProductTitle.waitForVisible(5000);
        global.basketTitle = this.basketProductTitle.getText();
        return this.basketProductTitle.getText();
    }

    // This method return the quantity of the product from basket page
    getBasketQuantity() {
        this.quantitySizeLabel.waitForVisible(5000);
        return (browser.elementIdText(this.quantitySizeLabel.value[0].ELEMENT).value).slice(0,1);
    }

    /*
    This method return the a boolean value based on comparing the product title fro Product Description page and Basket page which is stored in global variables.
    Before comparing the titles are converted to LowerCase as it was observed that some products are having case difference on PDP and Basket page (Eg. Small Flower And Tassel Earrings)
    */
    isProductTitleSame() {
        return assert.equal(global.pdpTitle.toLowerCase(), global.basketTitle.toLowerCase());
    }

    // This method compares the quantity of the product on basket page with the quantity selected on PDP (For now we have kept the quantity as static 3)
    isQuantitySame() {
        return assert.equal(3, this.getBasketQuantity());
    }

    // This method removes the product from the basket
    removeProduct() {
        this.deleteIcon.waitForVisible(5000);
        this.deleteIcon.click();
        this.deleteButtonPopUp.waitForVisible(5000);
        this.deleteButtonPopUp.click();
    }

    // This method checks if teh basket is empty by comparing the message displayed
    isBasketEmpty() {
        this.basketEmptyMsg.waitForVisible(5000);
        return assert.equal(this.basketEmptyMsg.getText(), "Your shopping bag is currently empty.")
    }
}

export default new basket();