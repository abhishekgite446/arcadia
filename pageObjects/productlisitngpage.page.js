import commonHelpers from './commonhelpers.page'

class productlistingpage {

    /* page elements */

    get resultTotal()   { return browser.element('.PlpHeader-total.PlpHeader-totalValue'); }
    get allProducts()   { return browser.elements('.Product.Product--col2');}

    /* page methods */

    // This method will return the total results text displayed for a given product search
    totalResults() {
        browser.pause(2000);
        this.resultTotal.waitForVisible(5000);
        return this.resultTotal.getText();
    }

    /*
    This method select a random product from the listing page. It initially will get an list for all the elements displayed
    then it will get random element from that list by using the getRandomNumber method. Then it will click on that object.
    It was rarely observed that a undefined element is returned when try to get a random element, in that case re-run the test.
    */
    clickOnRandomProduct() {
        var random = this.allProducts.value[commonHelpers.getRandomNumber(this.allProducts)]
        browser.pause(5000);
        browser.elementIdClick(random.ELEMENT);
    }
}

export default new productlistingpage();