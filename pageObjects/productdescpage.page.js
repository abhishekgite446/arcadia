import commonHelpers from './commonhelpers.page'

const assert = require('assert');
class productdescpage  {

    /* page elements */

    get productDetailContainer()   { return browser.element('.ProductDetail'); }
    get quantityDropDown() { return '#productQuantity' }
    get allQuantities() { return browser.elements('.Select-select--option ') }
    get productTitle() {return browser.element('.ProductDetail-title'); }
    get addToBagButton() {return browser.element('.Button'); }
    get viewBagButtonPopUp() {return browser.element('.Button.AddToBagConfirm-viewBag.Button--secondary.Button--twoFifthWidth'); }
    get productSize() {return browser.elements('.ProductSizes-item'); }
    get productSizeList() {return browser.element('.ProductSizes-list') }

    /* page methods */

    // This method will select a quantity 3 (It can be chnaged). Additionally if a product is selected which needs size selection then a first in-stock size will be selected.
    selectQuantity() {
        browser.pause(2000);
        // If the product needs size selection the following if statement will be executed
        if(this.productSizeList.isVisible()){
            console.log(this.productSize.value.length);
            browser.click('*//span[@class="ProductSizes-item"]');
        }
        browser.element(this.quantityDropDown).waitForVisible(5000);
        browser.scroll(this.quantityDropDown);
        browser.selectByValue(this.quantityDropDown, 3);
    }

    // This method will adda product to the bag
    addtoBag() {
        this.addToBagButton.click();
    }

    // This method will click on the 'View Bag' button from the pop-up
    viewBagPopUp() {
        this.viewBagButtonPopUp.waitForVisible(5000);
        this.viewBagButtonPopUp.click();
    }

    // This method will return the product title
    getProductTitle() {
        global.pdpTitle = this.productTitle.getText();
        return this.productTitle.getText();
    }
}

export default new productdescpage();