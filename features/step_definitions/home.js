import homePage from '../../pageObjects/homepage.page'
import productListingPage from '../../pageObjects/productlisitngpage.page'
import productDescPage from '../../pageObjects/productdescpage.page'
import commonHelpers from '../../pageObjects/commonhelpers.page'
import basket from '../../pageObjects/basket.page'
const assert = require('assert');

module.exports = function () {

    this.When(/^I am on the landing page$/,{timeout: 20 * 1000}, () => {
         homePage.goToPage()
    });

    this.Then(/^I can see logo in navigation bar$/, () => {
         commonHelpers.elementIsVisible(homePage.brandLogo);
    });

    this.When(/^I search for a random product$/, () => {
         homePage.searchProduct();
    });

    this.When(/^the product listing page should display a list of products$/, () => {
         assert.ok(productListingPage.totalResults() > 0)
    });

    this.When(/^I select a random product from the returned list$/, () => {
         productListingPage.clickOnRandomProduct();

    });

    this.Then(/^I should be taken to the product detail page$/, () => {
         commonHelpers.elementIsVisible(productDescPage.productDetailContainer);
    });

    this.Given(/^I change the product quantity$/, () => {
         productDescPage.selectQuantity();
    });

    this.When(/^I add the selected product to the basket$/, () => {
        productDescPage.getProductTitle();
        productDescPage.addtoBag();
        productDescPage.viewBagPopUp();
    });

    this.Then(/^I should see the product added to the basket$/, () => {
        basket.getBasketProductTitle();
        basket.isProductTitleSame();
    });

    this.Then(/^I should see the selected quantity for the product$/, () => {
        basket.isQuantitySame();
    });

    this.Given(/^I add a random product to the basket$/, () => {
        productListingPage.clickOnRandomProduct();
        commonHelpers.elementIsVisible(productDescPage.productDetailContainer);
        productDescPage.selectQuantity();
        productDescPage.addtoBag();
    });

    this.When(/^I open the basket and remove the added product$/, () => {
        productDescPage.viewBagPopUp();
        basket.removeProduct();
    });

    this.Then(/^my basket should be empty$/, () => {
        basket.isBasketEmpty();
    });
};
